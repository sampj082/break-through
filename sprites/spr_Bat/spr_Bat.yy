{
    "id": "76dc841a-cc8e-4011-8206-e23f700f38c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b72d6d45-3a98-4692-adba-d9bbfa8f4781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76dc841a-cc8e-4011-8206-e23f700f38c8",
            "compositeImage": {
                "id": "8d61c82f-8aad-4631-bc0b-aea06ef94cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b72d6d45-3a98-4692-adba-d9bbfa8f4781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21706016-a92e-4501-8134-c212794c8348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b72d6d45-3a98-4692-adba-d9bbfa8f4781",
                    "LayerId": "42eb4b0e-aaac-4c22-adbd-c5c307491e6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "42eb4b0e-aaac-4c22-adbd-c5c307491e6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76dc841a-cc8e-4011-8206-e23f700f38c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}