{
    "id": "06995705-8478-446a-ac5d-d003a12a9cb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Brick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4fed5ff-1730-4ccf-b264-c401ba7ca6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06995705-8478-446a-ac5d-d003a12a9cb7",
            "compositeImage": {
                "id": "7621b5fd-af74-4ef9-81a6-c5cbb9625cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4fed5ff-1730-4ccf-b264-c401ba7ca6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "434624d1-3c43-4b72-bfed-32bff34fccf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4fed5ff-1730-4ccf-b264-c401ba7ca6ed",
                    "LayerId": "a708c9c8-c40a-4bad-8087-c35301e3fcc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a708c9c8-c40a-4bad-8087-c35301e3fcc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06995705-8478-446a-ac5d-d003a12a9cb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}