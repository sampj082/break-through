{
    "id": "e8c2015a-3b3b-4cb3-aea4-7242df7f242b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr__Ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e0d0039-c70f-4da5-9979-4039ac5afc9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8c2015a-3b3b-4cb3-aea4-7242df7f242b",
            "compositeImage": {
                "id": "3f34769a-ec59-4136-8d6b-e447f56b9967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0d0039-c70f-4da5-9979-4039ac5afc9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fcad46b-0170-42df-8a01-bef027f3decf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0d0039-c70f-4da5-9979-4039ac5afc9b",
                    "LayerId": "f53bc9b2-a0c6-4f86-9cd6-6c2719f0f9b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "f53bc9b2-a0c6-4f86-9cd6-6c2719f0f9b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8c2015a-3b3b-4cb3-aea4-7242df7f242b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}