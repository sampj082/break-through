{
    "id": "c0e5a115-16ae-4791-aab0-ef64041b7f52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Brick",
    "eventList": [
        {
            "id": "3e8fcdf8-7103-42c3-8cda-66792799e9bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0e5a115-16ae-4791-aab0-ef64041b7f52"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "06995705-8478-446a-ac5d-d003a12a9cb7",
    "visible": true
}