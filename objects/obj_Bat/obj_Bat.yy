{
    "id": "0254db21-18d8-4b4b-973b-b70c03bf81a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bat",
    "eventList": [
        {
            "id": "394fd9d8-2c98-449a-b1d7-6f28fdcd3398",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0254db21-18d8-4b4b-973b-b70c03bf81a2"
        },
        {
            "id": "69f4caf2-af87-4e39-9e62-c82bfae7b256",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0254db21-18d8-4b4b-973b-b70c03bf81a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "76dc841a-cc8e-4011-8206-e23f700f38c8",
    "visible": true
}