{
    "id": "fb689b4d-3eb5-4508-9ab5-5036d9bb6b32",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Ball",
    "eventList": [
        {
            "id": "3bf9330c-0f58-4f4f-b9f0-06094b7c0e40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb689b4d-3eb5-4508-9ab5-5036d9bb6b32"
        },
        {
            "id": "ef1ac337-cbd6-44c0-af02-53c5679a7ce2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb689b4d-3eb5-4508-9ab5-5036d9bb6b32"
        },
        {
            "id": "ebc1fc58-8905-434e-8d9c-365f101433aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "fb689b4d-3eb5-4508-9ab5-5036d9bb6b32"
        },
        {
            "id": "e2682203-cd43-434b-82e2-c69f9f1c32f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c0e5a115-16ae-4791-aab0-ef64041b7f52",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fb689b4d-3eb5-4508-9ab5-5036d9bb6b32"
        },
        {
            "id": "862ab69f-086e-428a-94f0-e5aabe22a9bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0254db21-18d8-4b4b-973b-b70c03bf81a2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fb689b4d-3eb5-4508-9ab5-5036d9bb6b32"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e8c2015a-3b3b-4cb3-aea4-7242df7f242b",
    "visible": true
}